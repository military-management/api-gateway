package api

import (
	"gitlab.com/military-management/api-gateway/api/handler"
	"gitlab.com/military-management/api-gateway/api/middleware"

	"github.com/gin-gonic/gin"
	files "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @title Military Management
// @version 1.0
// @description Military Management
// @host localhost:8080
// @BasePath /
// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization
func NewGin(h *handler.Handler) *gin.Engine {

	r := gin.Default()

	r.Use(middleware.MiddleWare())
	url := ginSwagger.URL("swagger/doc.json")
	r.GET("/swagger/*any", ginSwagger.WrapHandler(files.Handler, url))

	u := r.Group("/user")
	u.POST("/register", h.Register)
	u.GET("/refreshTokenRequest", h.RefreshToken)
	u.POST("/login", h.Login)
	u.DELETE("/logout", h.Logout)

	ai := r.Group("/ai-recommend")
	ai.POST("/create", h.CreateAdvice)
	ai.GET("/getall", h.GetAllReqs)
	ai.GET("/get/advice/:advice_id", h.GetAdvice)
	ai.GET("/get/soldier/:soldier_id", h.GetSoldierReq)

	rc := r.Group("/resources")
	rc.POST("/ammo/add", h.AddAmmo)
	rc.GET("/ammo", h.GetAmmo)
	rc.POST("/artillery/add", h.AddArtilleriya)
	rc.GET("/artillery", h.GetArtilleriya)
	rc.POST("/fuel/add", h.AddFuel)
	rc.GET("/fuel", h.GetFuel)
	rc.POST("/giverecource", h.GivedReecure)
	rc.POST("/backrecource", h.BackRecurse)
	rc.POST("/group/usage", h.ViewGroupSpell)

	s := r.Group("/soldier")
	s.POST("/add", h.AddSoldier)
	s.GET("/getall", h.GetAllSoldiers)
	s.GET("/getbyid/:id", h.GetSoldierById)
	s.PUT("/update/", h.UpdateSoldier)
	s.DELETE("/delete/:id", h.DeleteSoldier)
	s.GET("/list", h.ListSoldiers)
	s.GET("/filter/age", h.FilterSoldiersByAge)
	s.GET("/filter/serviceend", h.ListSoldiersByServiceEndDate)
	s.GET("/filter/joindate", h.FilterSoldiersByJoinDate)
	s.GET("/filter/ammunition", h.TrackAmmunitionUsage)

	return r
}
