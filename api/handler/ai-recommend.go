package handler

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	pb "gitlab.com/military-management/api-gateway/genprotos/ai"
)

// AdviceRequestInput represents the required input for creating an advice.
type AdviceRequestInput struct {
	SoldierId string `json:"soldier_id" binding:"required"`
	Context   string `json:"context" binding:"required"`
}

// CreateAdvice handles the creation of a new Ai Advice
// @Summary Create Ai Advice
// @Description Create a new Ai Advice
// @Tags Ai Advice
// @Accept json
// @Produce json
// @Security BearerAuth
// @Param CreateAdvice body AdviceRequestInput true "Ai Advice"
// @Success 200 {object} pb.AdviceResponse "Ai Advice Successful"
// @Failure 400 {string} string "Invalid request format"
// @Failure 400 {string} string "Missing required fields"
// @Failure 500 {string} string "Failed to create advice"
// @Failure 500 {string} string "Invalid response from AI service"
// @Router /ai-recommend/create [post]
func (h *Handler) CreateAdvice(c *gin.Context) {
	var input AdviceRequestInput
	if err := c.BindJSON(&input); err != nil {
		log.Printf("Error binding JSON: %v", err)
		c.String(http.StatusBadRequest, "Invalid request format")
		return
	}
	if input.SoldierId == "" || input.Context == "" {
		log.Println("Error: Missing required fields")
		c.String(http.StatusBadRequest, "Missing required fields")
		return
	}


	advice := pb.AdviceRequest{
		SoldierId: input.SoldierId,
		Context:   input.Context,
		AdviceId:  uuid.NewString(),
	}

	res, err := h.Ai.CreateAdvice(context.Background(), &advice)
	if err != nil {
		log.Printf("Error calling CreateAdvice: %v", err)
		c.String(http.StatusInternalServerError, fmt.Sprintf("Failed to create advice: %v", err))
		return
	}

	if res == nil {
		log.Println("Error: Invalid response from AI service")
		c.String(http.StatusInternalServerError, "Invalid response from AI service")
		return
	}

	c.JSON(http.StatusOK, res)
}

// GetAdvice handles the retrieval of a specific Ai Advice
// @Summary Get Ai Advice
// @Description Get a specific Ai Advice
// @Tags Ai Advice
// @Accept json
// @Produce json
// @Security BearerAuth
// @Param advice_id path string true "Advice ID"
// @Success 200 {object} pb.AdviceResponse "Advice Successful"
// @Failure 400 {string} string "Error while retrieving Ai Advice"
// @Router /ai-recommend/get/advice/{advice_id} [get]
func (h *Handler) GetAdvice(c *gin.Context) {
	adviceID := c.Param("advice_id")
	advice := pb.GetAdviceRequest{AdviceId: adviceID}
	res, err := h.Ai.GetAdvice(context.Background(), &advice)
	if err != nil {
		c.String(http.StatusBadRequest, "Failed to get advice")
		return
	}
	c.JSON(http.StatusOK, res)
}

// GetSoldierReq handles the retrieval of Ai Advices for a specific soldier
// @Summary Get Soldier Ai Advices
// @Description Get Ai Advices for a specific soldier
// @Tags Ai Advice
// @Accept json
// @Produce json
// @Security BearerAuth
// @Param soldier_id path string true "Soldier ID"
// @Success 200 {object} pb.SoldierRequests "Advices Successful"
// @Failure 400 {string} string "Error while retrieving Ai Advices"
// @Router /ai-recommend/get/soldier/{soldier_id} [get]
func (h *Handler) GetSoldierReq(c *gin.Context) {
	soldierID := c.Param("soldier_id")
	advice := pb.SoldierRequests{SoldierId: soldierID}
	res, err := h.Ai.GetSoldierReq(context.Background(), &advice)
	if err != nil {
		c.String(http.StatusBadRequest, "Failed to get soldier advices")
		return
	}
	c.JSON(http.StatusOK, res)
}

// GetAllReqs handles the retrieval of all Ai Advices
// @Summary Get All Ai Advices
// @Description Get all Ai Advices
// @Tags Ai Advice
// @Accept json
// @Produce json
// @Security BearerAuth
// @Success 200 {object} pb.GetAllSoldiersResp "All Advices Successful"
// @Failure 400 {string} string "Error while retrieving all Ai Advices"
// @Router /ai-recommend/getall [get]
func (h *Handler) GetAllReqs(c *gin.Context) {
	res, err := h.Ai.GetAllReqs(context.Background(), &pb.Empty{})
	if err != nil {
		c.String(http.StatusBadRequest, "Failed to get all advices")
		return
	}

	c.JSON(http.StatusOK, res)
}
