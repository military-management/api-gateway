package handler

import (
	pb "gitlab.com/military-management/api-gateway/genprotos/auth"
	pba "gitlab.com/military-management/api-gateway/genprotos/ai"
	rc "gitlab.com/military-management/api-gateway/genprotos/recurece"
	pbs "gitlab.com/military-management/api-gateway/genprotos/soldier"
)

type Handler struct {
	Auth pb.AuthServiceClient
	Ai   pba.AdviceServiceClient
	RecourseRepo rc.RecursesClient
	Soldier pbs.SoldierServiceClient
}

func NewHandler(auth pb.AuthServiceClient, ai pba.AdviceServiceClient,rc rc.RecursesClient, pbs pbs.SoldierServiceClient) *Handler {
	return &Handler{
		Auth: auth,
		Ai:   ai,
		RecourseRepo: rc,
		Soldier: pbs,
	}
}