package handler

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	pb "gitlab.com/military-management/api-gateway/genprotos/recurece"
)

// AddAmmo handles the addition of ammo
// @Summary Add Ammo
// @Description Add ammo to the resources
// @Tags Resources
// @Accept json
// @Produce json
// @Security BearerAuth
// @Param AddAmmoReq body pb.AddAmmoReq true "Add Ammo Request"
// @Success 200 {object} pb.AddAmmoRes "Add Ammo Response"
// @Failure 400 {object} string "Error while adding ammo"
// @Router /resources/ammo/add [post]
func (h *Handler) AddAmmo(ctx *gin.Context) {
	req := &pb.AddAmmoReq{}
	if err := ctx.BindJSON(req); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	res, err := h.RecourseRepo.AddAmmo(context.Background(), req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// GetAmmo handles retrieving the current amount of ammo
// @Summary Get Ammo
// @Description Get current ammo from resources
// @Tags Resources
// @Accept json
// @Produce json
// @Security BearerAuth
// @Success 200 {object} pb.GetAmmoRes "Get Ammo Response"
// @Failure 400 {object} string "Error while getting ammo"
// @Router /resources/ammo [get]
func (h *Handler) GetAmmo(ctx *gin.Context) {
	req := &pb.GetAmmoReq{}
	res, err := h.RecourseRepo.GetAmmo(context.Background(), req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// AddArtilleriya handles the addition of artillery
// @Summary Add Artillery
// @Description Add artillery to the resources
// @Tags Resources
// @Accept json
// @Produce json
// @Security BearerAuth
// @Param AddArtilleriyaReq body pb.AddArtilleriyaReq true "Add Artillery Request"
// @Success 200 {object} pb.AddArtilleriyaRes "Add Artillery Response"
// @Failure 400 {object} string "Error while adding artillery"
// @Router /resources/artillery/add [post]
func (h *Handler) AddArtilleriya(ctx *gin.Context) {
	req := &pb.AddArtilleriyaReq{}
	if err := ctx.BindJSON(req); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	res, err := h.RecourseRepo.AddArtilleriya(context.Background(), req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// GetArtilleriya handles retrieving the current amount of artillery
// @Summary Get Artillery
// @Description Get current artillery from resources
// @Tags Resources
// @Accept json
// @Produce json
// @Security BearerAuth
// @Success 200 {object} pb.GetArtilleriyaRes "Get Artillery Response"
// @Failure 400 {object} string "Error while getting artillery"
// @Router /resources/artillery [get]
func (h *Handler) GetArtilleriya(ctx *gin.Context) {
	req := &pb.GetArtilleriyaReq{}
	res, err := h.RecourseRepo.GetArtilleriya(context.Background(), req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// AddFuel handles the addition of fuel
// @Summary Add Fuel
// @Description Add fuel to the resources
// @Tags Resources
// @Accept json
// @Produce json
// @Security BearerAuth
// @Param AddFuelReq body pb.AddFuelReq true "Add Fuel Request"
// @Success 200 {object} pb.AddFuelRes "Add Fuel Response"
// @Failure 400 {object} string "Error while adding fuel"
// @Router /resources/fuel/add [post]
func (h *Handler) AddFuel(ctx *gin.Context) {
	req := &pb.AddFuelReq{}
	if err := ctx.BindJSON(req); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	res, err := h.RecourseRepo.AddFuel(context.Background(), req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// GetFuel handles retrieving the current amount of fuel
// @Summary Get Fuel
// @Description Get current fuel from resources
// @Tags Resources
// @Accept json
// @Produce json
// @Security BearerAuth
// @Success 200 {object} pb.GetFuelRes "Get Fuel Response"
// @Failure 400 {object} string "Error while getting fuel"
// @Router /resources/fuel [get]
func (h *Handler) GetFuel(ctx *gin.Context) {
	req := &pb.GetFuelReq{}
	res, err := h.RecourseRepo.GetFuel(context.Background(), req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// GivedReecure handles assigning resources to soldiers
// @Summary Give Resource
// @Description Assign resources to a soldier
// @Tags Resources
// @Accept json
// @Produce json
// @Security BearerAuth
// @Param GivedReecureReq body pb.GivedReecureReq true "Give Resource Request"
// @Success 200 {object} pb.GivedReecureRes "Give Resource Response"
// @Failure 400 {object} string "Error while giving resource"
// @Router /resources/giverecource [post]
func (h *Handler) GivedReecure(ctx *gin.Context) {
	req := &pb.GivedReecureReq{}
	if err := ctx.BindJSON(req); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	res, err := h.RecourseRepo.GivedReecure(context.Background(), req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// BackRecurse handles returning resources from soldiers
// @Summary Return Resource
// @Description Return resources from a soldier
// @Tags Resources
// @Accept json
// @Produce json
// @Security BearerAuth
// @Param BackRecurseReq body pb.BackRecurseReq true "Return Resource Request"
// @Success 200 {object} pb.BackRecurseRes "Return Resource Response"
// @Failure 400 {object} string "Error while returning resource"
// @Router /resources/backrecource [post]
func (h *Handler) BackRecurse(ctx *gin.Context) {
	req := &pb.BackRecurseReq{}
	if err := ctx.BindJSON(req); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	res, err := h.RecourseRepo.BackRecurse(context.Background(), req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// ViewGroupSpell handles viewing the resources of a group
// @Summary View Group Resource Usage
// @Description View the resource usage of a group
// @Tags Resources
// @Accept json
// @Produce json
// @Security BearerAuth
// @Param ViewGroupSpellReq body pb.ViewGroupSpellReq true "View Group Resource Usage Request"
// @Success 200 {object} pb.ViewGroupSpellRes "View Group Resource Usage Response"
// @Failure 400 {object} string "Error while viewing group resource usage"
// @Router /resources/group/usage [post]
func (h *Handler) ViewGroupSpell(ctx *gin.Context) {
	req := &pb.ViewGroupSpellReq{}
	if err := ctx.BindJSON(req); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	res, err := h.RecourseRepo.ViewGroupSpell(context.Background(), req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}
