package handler

import (
	"context"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	pb "gitlab.com/military-management/api-gateway/genprotos/soldier"
)

// AddSoldier handles the creation of a new soldier.
// @Summary Add Soldier
// @Description Add Soldier
// @Tags Soldier
// @Accept json
// @Produce json
// @Security BearerAuth
// @Param soldier body pb.AddSoldierRequest true "Add Soldier"
// @Success 200 {object} pb.AddSoldierResponse "Add Soldier Successful"
// @Failure 400 {string} string "Error while adding soldier"
// @Router /soldier/add [post]
func (h *Handler) AddSoldier(c *gin.Context) {
	var soldier pb.AddSoldierRequest
	if err := c.ShouldBindJSON(&soldier); err != nil {
		log.Printf("Error binding JSON: %v", err)
		c.String(http.StatusBadRequest, "Invalid request format")
		return
	}
	res, err := h.Soldier.AddSoldier(context.Background(), &soldier)
	if err != nil {
		log.Printf("Failed to add soldier: %v", err)
		c.String(http.StatusBadRequest, "Failed to add soldier")
		return
	}
	c.JSON(http.StatusOK, res)
}

// GetSoldierById handles retrieving a soldier by ID.
// @Summary Get Soldier by ID
// @Description Get Soldier by ID
// @Tags Soldier
// @Produce json
// @Param id path string true "Soldier ID"
// @Success 200 {object} pb.Soldier "Get Soldier Successful"
// @Failure 404 {string} string "Soldier not found"
// @Router /soldier/getbyid/{id} [get]
func (h *Handler) GetSoldierById(c *gin.Context) {
	id := c.Param("id")
	req := &pb.GetSoldierRequest{Id: id}
	res, err := h.Soldier.GetSoldierById(context.Background(), req)
	if err != nil {
		log.Printf("Failed to get soldier: %v", err)
		c.String(http.StatusNotFound, "Soldier not found")
		return
	}
	c.JSON(http.StatusOK, res)
}

// GetAllSoldiers handles retrieving all soldiers.
// @Summary Get All Soldiers
// @Description Get All Soldiers
// @Tags Soldier
// @Produce json
// @Success 200 {object} pb.ListSoldiersResponse "Get All Soldiers Successful"
// @Failure 500 {string} string "Error while retrieving soldiers"
// @Router /soldiers/getall [get]
func (h *Handler) GetAllSoldiers(c *gin.Context) {
	req := &pb.GetAllSoldiersRequest{}
	res, err := h.Soldier.GetAllSoldiers(context.Background(), req)
	if err != nil {
		log.Printf("Failed to get all soldiers: %v", err)
		c.String(http.StatusInternalServerError, "Error while retrieving soldiers")
		return
	}
	c.JSON(http.StatusOK, res)
}

// UpdateSoldier handles updating an existing soldier.
// @Summary Update Soldier
// @Description Update Soldier
// @Tags Soldier
// @Accept json
// @Produce json
// @Security BearerAuth
// @Param soldier body pb.UpdateSoldierRequest true "Update Soldier"
// @Success 200 {object} pb.UpdateSoldierResponse "Update Soldier Successful"
// @Failure 400 {string} string "Error while updating soldier"
// @Router /soldier/update [put]
func (h *Handler) UpdateSoldier(c *gin.Context) {
	var soldier pb.UpdateSoldierRequest
	if err := c.ShouldBindJSON(&soldier); err != nil {
		log.Printf("Error binding JSON: %v", err)
		c.String(http.StatusBadRequest, "Invalid request format")
		return
	}
	res, err := h.Soldier.UpdateSoldier(context.Background(), &soldier)
	if err != nil {
		log.Printf("Failed to update soldier: %v", err)
		c.String(http.StatusBadRequest, "Failed to update soldier")
		return
	}
	c.JSON(http.StatusOK, res)
}

// DeleteSoldier handles deleting a soldier by ID.
// @Summary Delete Soldier
// @Description Delete Soldier
// @Tags Soldier
// @Produce json
// @Param id path string true "Soldier ID"
// @Success 200 {object} pb.DeleteSoldierResponse "Delete Soldier Successful"
// @Failure 400 {string} string "Error while deleting soldier"
// @Router /soldier/delete/{id} [delete]
func (h *Handler) DeleteSoldier(c *gin.Context) {
	id := c.Param("id")
	req := &pb.DeleteSoldierRequest{Id: id}
	res, err := h.Soldier.DeleteSoldier(context.Background(), req)
	if err != nil {
		log.Printf("Failed to delete soldier: %v", err)
		c.String(http.StatusBadRequest, "Failed to delete soldier")
		return
	}
	c.JSON(http.StatusOK, res)
}

// ListSoldiers handles listing soldiers with optional filters.
// @Summary List Soldiers
// @Description List Soldiers
// @Tags Soldier
// @Produce json
// @Param min_age query int false "Minimum Age"
// @Param max_age query int false "Maximum Age"
// @Param start_date query string false "Start Date (YYYY-MM-DD)"
// @Param end_date query string false "End Date (YYYY-MM-DD)"
// @Success 200 {object} pb.ListSoldiersResponse "List Soldiers Successful"
// @Failure 500 {string} string "Error while listing soldiers"
// @Router /soldiers/list [get]
func (h *Handler) ListSoldiers(c *gin.Context) {
	req := &pb.ListSoldiersRequest{
		Filters: &pb.FilterCriteria{
			MinAge:    0,
			MaxAge:    0,
			StartDate: c.Query("start_date"),
			EndDate:   c.Query("end_date"),
		},
	}
	if minAge, err := strconv.Atoi(c.Query("min_age")); err == nil {
		req.Filters.MinAge = int32(minAge)
	}
	if maxAge, err := strconv.Atoi(c.Query("max_age")); err == nil {
		req.Filters.MaxAge = int32(maxAge)
	}

	res, err := h.Soldier.ListSoldiers(context.Background(), req)
	if err != nil {
		log.Printf("Failed to list soldiers: %v", err)
		c.String(http.StatusInternalServerError, "Error while listing soldiers")
		return
	}
	c.JSON(http.StatusOK, res)
}

// FilterSoldiersByAge handles filtering soldiers by age range.
// @Summary Filter Soldiers by Age
// @Description Filter Soldiers by Age
// @Tags Soldier
// @Produce json
// @Param min_age query int true "Minimum Age"
// @Param max_age query int true "Maximum Age"
// @Success 200 {object} pb.ListSoldiersResponse "Filter Soldiers by Age Successful"
// @Failure 500 {string} string "Error while filtering soldiers"
// @Router /soldiers/filter/age [get]
func (h *Handler) FilterSoldiersByAge(c *gin.Context) {
	minAge, err := strconv.Atoi(c.Query("min_age"))
	if err != nil {
		c.String(http.StatusBadRequest, "Invalid min_age")
		return
	}
	maxAge, err := strconv.Atoi(c.Query("max_age"))
	if err != nil {
		c.String(http.StatusBadRequest, "Invalid max_age")
		return
	}
	req := &pb.FilterSoldiersByAgeRequest{
		MinAge: int32(minAge),
		MaxAge: int32(maxAge),
	}
	res, err := h.Soldier.FilterSoldiersByAge(context.Background(), req)
	if err != nil {
		log.Printf("Failed to filter soldiers by age: %v", err)
		c.String(http.StatusInternalServerError, "Error while filtering soldiers")
		return
	}
	c.JSON(http.StatusOK, res)
}

// FilterSoldiersByJoinDate handles filtering soldiers by join date range.
// @Summary Filter Soldiers by Join Date
// @Description Filter Soldiers by Join Date
// @Tags Soldier
// @Produce json
// @Param start_date query string true "Start Date (YYYY-MM-DD)"
// @Param end_date query string true "End Date (YYYY-MM-DD)"
// @Success 200 {object} pb.ListSoldiersResponse "Filter Soldiers by Join Date Successful"
// @Failure 500 {string} string "Error while filtering soldiers"
// @Router /soldiers/filter/joindate [get]
func (h *Handler) FilterSoldiersByJoinDate(c *gin.Context) {
	req := &pb.FilterSoldiersByJoinDateRequest{
		StartDate: c.Query("start_date"),
		EndDate:   c.Query("end_date"),
	}
	res, err := h.Soldier.FilterSoldiersByJoinDate(context.Background(), req)
	if err != nil {
		log.Printf("Failed to filter soldiers by join date: %v", err)
		c.String(http.StatusInternalServerError, "Error while filtering soldiers")
		return
	}
	c.JSON(http.StatusOK, res)
}

// ListSoldiersByServiceEndDate handles listing soldiers by service end date.
// @Summary List Soldiers by Service End Date
// @Description List Soldiers by Service End Date
// @Tags Soldier
// @Produce json
// @Param end_date query string true "Service End Date (YYYY-MM-DD)"
// @Success 200 {object} pb.ListSoldiersResponse "List Soldiers by Service End Date Successful"
// @Failure 500 {string} string "Error while listing soldiers"
// @Router /soldiers/serviceend [get]
func (h *Handler) ListSoldiersByServiceEndDate(c *gin.Context) {
    req := &pb.ListSoldiersByServiceEndDateRequest{
        EndDate: c.Query("end_date"),
    }
    res, err := h.Soldier.ListSoldiersByServiceEndDate(context.Background(), req)
    if err != nil {
        log.Printf("Failed to list soldiers by service end date: %v", err)
        c.String(http.StatusInternalServerError, "Error while listing soldiers")
        return
    }
    c.JSON(http.StatusOK, res)
}

// TrackAmmunitionUsage handles tracking the ammunition usage of soldiers.
// @Summary Track Ammunition Usage
// @Description Track Ammunition Usage
// @Tags Soldier
// @Accept json
// @Produce json
// @Security BearerAuth
// @Param ammunition body pb.TrackAmmunitionUsageRequest true "Track Ammunition Usage"
// @Success 200 {object} pb.TrackAmmunitionUsageResponse "Track Ammunition Usage Successful"
// @Failure 400 {string} string "Error while tracking ammunition usage"
// @Router /ammunition/track [post]
func (h *Handler) TrackAmmunitionUsage(c *gin.Context) {
	var ammo pb.TrackAmmunitionUsageRequest
	if err := c.ShouldBindJSON(&ammo); err != nil {
		log.Printf("Error binding JSON: %v", err)
		c.String(http.StatusBadRequest, "Invalid request format")
		return
	}
	res, err := h.Soldier.TrackAmmunitionUsage(context.Background(), &ammo)
	if err != nil {
		log.Printf("Failed to track ammunition usage: %v", err)
		c.String(http.StatusBadRequest, "Failed to track ammunition usage")
		return
	}
	c.JSON(http.StatusOK, res)
}
