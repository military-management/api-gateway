package handler

import (
	"log"
	"net/http"

	pb "gitlab.com/military-management/api-gateway/genprotos/auth"

	"github.com/gin-gonic/gin"
	"gitlab.com/military-management/api-gateway/api/token"
)

// Register 		handles the creation of a new Register
// @Summary 		Create Register
// @Description 	Create page
// @Tags 			Users
// @Accept  		json
// @Produce  		json
// @Security  		BearerAuth
// @Param   		Create  body    pb.RegisterRequest    true   "Create"
// @Success 		200   {string}  string    "Create Successful"
// @Failure 		401   {string}  string    "Error while Created"
// @Router 			/user/register [post]
func (h *Handler) Register(ctx *gin.Context) {
	arr := &pb.RegisterRequest{}
	if err := ctx.BindJSON(arr); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	arr.Role = "user"
	t := token.GenereteJWTToken(arr)
	arr.Token = t.RefreshToken
	res, err := h.Auth.Register(ctx, arr)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	res.RefreshToken = t.RefreshToken
	res.AccessToken = t.AccessToken
	ctx.JSON(http.StatusOK, res)
}

// Logout 			handles the creation of a new Logout
// @Summary 		Delete Logout
// @Description 	Delete page
// @Tags 			Users
// @Accept  		json
// @Produce  		json
// @Security  		BearerAuth
// @Param   		Logout  body    pb.LogoutRequest    true   "Logout"
// @Success 		200 {string}  string  "Logout Successful"
// @Failure 		401 {string}  string  "Error while Deleted"
// @Router 			/user/logout [delete]
func (h *Handler) Logout(ctx *gin.Context) {
	arr := pb.LogoutRequest{}
	err := ctx.BindJSON(&arr)
	if err != nil {
		log.Fatal(err)
	}
	res, err := h.Auth.Logout(ctx, &arr)
	if err != nil {
		panic(err)
	}
	ctx.JSON(200, res)
}

// Login 		    handles the creation of a new Login
// @Summary 		Login
// @Description 	Create page
// @Tags 			Users
// @Accept  		json
// @Produce  		json
// @Security  		BearerAuth
// @Param   		Create  body    pb.LoginRequest    true   "Create"
// @Success 		200   {string}  string    "Create Successful"
// @Failure 		401   {string}  string    "Error while Created"
// @Router 			/user/login [post]
func (h *Handler) Login(ctx *gin.Context) {
	arr := pb.LoginRequest{}
	err := ctx.BindJSON(&arr)
	if err != nil {
		panic(err)
	}
	res, err := h.Auth.Login(ctx, &arr)
	if err != nil {
		panic(err)
	}
	if !res.Success {
		ctx.JSON(200, res)
		return
	}
	registr := &pb.RegisterRequest{Username: arr.Username, Role: res.Role}
	t := token.GenereteJWTToken(registr)
	res.RefreshToken = t.RefreshToken
	res.AccessToken = t.AccessToken
	ctx.JSON(200, res)
}

// Refreshtoken		handles the creation of a new Login
// @Summary 		Login
// @Description 	Create page
// @Tags 			Users
// @Accept  		json
// @Produce  		json
// @Security  		BearerAuth
// @Param   		query query   pb.RefreshTokenRequest    true   "Create"
// @Success 		200   {string}  pb.RefreshTokenRequest   "Successful"
// @Failure 		401   {string}  string    "Error while Created"
// @Router 			/user/refreshTokenRequest [get]
func (h *Handler) RefreshToken(ctx *gin.Context) {
	arr := pb.RefreshTokenRequest{}
	arr.Token = ctx.Query("token")
	res, err := h.Auth.RefreshToken(ctx, &arr)
	if err != nil {
		panic(err)
	}
	// claims, err := token.ExtractClaim(res.NewRefreshToken)
	// if err != nil {
	// 	panic(err)
	// }
	// user := &pb.RegisterRequest{}
	// user.Username = claims["username"].(string)
	// user.Email = claims["email"].(string)
	// tok := token.GenereteJWTToken(user)
	// res.NewAccessToken = tok.AccessToken
	// res.NewRefreshToken = tok.RefreshToken
	ctx.JSON(200, res)
}
