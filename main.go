package main

import (
	"fmt"
	"log"

	api "gitlab.com/military-management/api-gateway/api"
	"gitlab.com/military-management/api-gateway/api/handler"
	pba "gitlab.com/military-management/api-gateway/genprotos/ai"
	pb "gitlab.com/military-management/api-gateway/genprotos/auth"
	rc "gitlab.com/military-management/api-gateway/genprotos/recurece"
	pbs "gitlab.com/military-management/api-gateway/genprotos/soldier"

	_ "gitlab.com/military-management/api-gateway/docs"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	UserConn, err := grpc.NewClient(fmt.Sprintf("localhost%s", ":8085"), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal("Error while Newclient: ", err.Error())
	}
	defer UserConn.Close()

	UpaymentConn, err := grpc.NewClient(fmt.Sprintf("localhost%s", ":8086"), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal("Error while NEwclient: ", err.Error())
	}

	Cityzen, err := grpc.NewClient(fmt.Sprintf("localhost%s", ":50050"), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal("Error while NEwclient: ", err.Error())
	}
	Recource, err := grpc.NewClient(fmt.Sprintf("localhost%s", ":8081"), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal("Error while NEwclient: ", err.Error())
	}

	Solider, err := grpc.NewClient(fmt.Sprintf("localhost%s", ":8090"), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal("Error while NEwclient: ", err.Error())
	}

	defer UpaymentConn.Close()
	us := pb.NewAuthServiceClient(UserConn)
	ps := pba.NewAdviceServiceClient(Cityzen)
	rc := rc.NewRecursesClient(Recource)
	pbs := pbs.NewSoldierServiceClient(Solider)

	h := handler.NewHandler(us, ps,rc, pbs)
	r := api.NewGin(h)

	fmt.Println("Server started on port:8080")
	err = r.Run(":8080")
	if err != nil {
		log.Fatal("Error while running server: ", err.Error())
	}
}
